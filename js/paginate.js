import { Previewer, registerHandlers } from 'pagedjs';
import { HandlerSkeleton } from "./HandlerSkeleton.js";

//pagination intitation method
export async function paginate(elementsList, styles) {

    const documentFragment = document.createDocumentFragment();

    //instanciate a Previewer to use,
    //Previewer {settings: {…}, polisher: Polisher, chunker: Chunker, hooks: {…}, size: {…}}
    const paged = new Previewer();
    
    //register a handler to define hooks on a specific method it defines
    //registerHandlers(HandlerSkeleton);

    //!important! forEach can't be used as it doesn't respect await order!
    for (let index = 0; index < elementsList.length; index++) {
        const element = elementsList[index];
        //populate document fragment
        documentFragment.appendChild(element);
    };

    //execute pagedjs preview
    //clear the body out
    document.body.innerHTML = "";
    const stylesToErase = [...document.head.getElementsByTagName("style")];
    stylesToErase.forEach( (style) => {
        style.remove();
    });
    paged.preview(documentFragment, styles, document.body);
    //paged.preview(documentFragment, null, document.body);
}
