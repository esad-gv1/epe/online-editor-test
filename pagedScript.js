import { paginate } from "./js/paginate.js";
import { smoothScroll } from "./js/utils.js";

const pagedjsOptions = document.getElementById("pagedjs-options");
let scrollMemory = {};
let recordScroll = true;

const styles = [
    "vendors/css/paged-preview.css",
    "css/print/print.css",
    "css/print/cover.css",
    "css/print/back.css"
];

//message reception
window.addEventListener("message", async (event) => {
    //console.log("message", event.source, event.data);

    if (event.data) {

        recordScroll = false;
        //Désérialiser les données
        const serializedElements = JSON.parse(event.data);
        //console.log("serializedElements", serializedElements)
        //Reconstruire les éléments HTML
        const elements = serializedElements.map(htmlString => {
            const tempDiv = document.createElement('div');
            tempDiv.innerHTML = htmlString;
            return tempDiv.firstChild;
        });
        //pagination with Paged.js
        await paginate(elements, styles);
        //get options back
        document.body.appendChild(pagedjsOptions);
        document.head.appendChild(styleSheet);

        const checked = doublePagesCheckBt.checked;
        if (checked) {
            styleSheet.disabled = true;
        }
        else {
            styleSheet.disabled = false;
        }
        
        //scroll
        setTimeout(() => {
            recordScroll = true;
            smoothScroll(scrollMemory.x, scrollMemory.y, 200);
        }, 100);
    }
});

const doublePagesCheckBt = document.getElementById("double-pages");
const styleSheet = document.getElementById("single-page-style");
//console.log("styleSheet", styleSheet);
doublePagesCheckBt.addEventListener("change", (event) => {
    const checked = event.target.checked;

    if (checked) {
        styleSheet.disabled = true;
    }
    else {
        styleSheet.disabled = false;
    }
});

const baselineCheckBt = document.getElementById("baseline");
baselineCheckBt.addEventListener("change", (event) => {
    const checked = event.target.checked;
    if (checked) {
        document.documentElement.style.setProperty("--baseline-color", "cyan");
        console.log("cyan");
    }
    else {
        document.documentElement.style.setProperty("--baseline-color", "tranparent");
        console.log("tranparent");
    }
});

window.addEventListener("beforeprint", (event) => {
    console.log("beforeprint");
    pagedjsOptions.style.display = "none";
});

window.addEventListener("afterprint", (event) => {
    console.log("afterprint");
    pagedjsOptions.style.display = "block";
});

//window.parent.postMessage({});

window.addEventListener("scroll", (event) => {
    if (recordScroll) {
        scrollMemory = { x: window.scrollX, y: window.scrollY };
        //console.log(scrollMemory);
    }
});