# EPE templates

## Online Editor test

![](screenshot.png)

This prototype has been developped after the workshop propagation #3 that was held in the Izmir University of economics, Visual Communication Department on May 13th-17th 2024.

It's a proof of concept aiming to facilitate the tecnical initation process for students that would like to invest web-to-print current technologies.

The window is sperated in 2 parts. On the left, an instance of Tiny-MDE, a premade Markdown editor ready to be used in the browser. Linked to Markdown-it, a mixture of HTML and Markdown can be used by the user.

The Preview button give the possibility to transfer the current Tiny-MDE editor content to the right part of the window, that consist of an iFrame prepared with a Paged.js instance. The layouted version is then displayed in this iFrame.

The Print button will send the order to the Paged.js iFrame to call the browser print dialog, and save the PDF for further printing.

## Usage

Clone the repo and run it from a sever.

## Technical choices

This project is meant to be run as it is from a browser, like [advancedTemplateES6](https://gitlab.com/esad-gv1/epe/advancedtemplatees6). It could be redesigned to use a development environment like `Vite`, in the same fashion that [vitees6template](https://gitlab.com/esad-gv1/epe/vitees6template).

Keep in mind that this is a simple proof of concept that is not meant to used in production.

```
.
├── css
│   ├── fonts
│   │   └── Inter-VariableFont_slnt_wght.ttf
│   ├── print
│   │   ├── back.css
│   │   ├── cover.css
│   │   └── print.css
│   └── screen
│       ├── back.css
│       ├── cover.css
│       └── main.css
├── index.html
├── js
│   ├── HandlerSkeleton.js
│   ├── paginate.js
│   └── utils.js
├── md
│   ├── back.md
│   ├── cover.md
│   └── main.md
├── paged.html
├── pagedScript.js
├── readMe.md
├── script.js
└── vendors
    ├── css
    │   ├── paged-preview.css
    │   └── tiny-mde.css
    └── js
        ├── markdown-it-attrs.js
        ├── markdown-it-bracketed-spans.js
        ├── markdown-it-container.js
        ├── markdown-it-footnote.js
        ├── markdown-it-span.js
        ├── markdown-it.js
        ├── paged.esm.js
        └── tiny-mde.js
```

## Roadmap

* Make CSS dedicated to the print part (right side of the window) lively editable.
* Enhance UI.
* Implement a saving system.
* Test an Electron native App that could be distributed

## Credits

* [Markdown-it](https://www.npmjs.com/package/markdown-it) for converting markdown to html in the browser;
* [Paged.js](https://pagedjs.org) for printed page layout and pdf generation;
* [Tiny-MDE](https://github.com/jefago/tiny-markdown-editor) as a simple Markdown realtime editor ready to use.

## Authors

Dominique Cunin
