import { loadScripts } from "./js/utils.js";

//pagedjs reserved iframe to rendrer the paginated version
const pagedjsFrame = document.getElementById("paged-preview-frame");
const previewPrintBt = document.getElementById("previewPrintBt");
const printBt = document.getElementById("printBt");

const tinyMDE = new TinyMDE.Editor({ element: "editor" });
const commandBar = new TinyMDE.CommandBar({
    element: "toolbar",
    editor: tinyMDE,
});

tinyMDE.addEventListener("change", (event) => {
    console.log(event);
});

const elementsToPaginate = [];

//additional script to load, not importable as es modules
const scritpList = [
    "vendors/js/markdown-it.js",
    "vendors/js/markdown-it-footnote.js",
    "vendors/js/markdown-it-attrs.js",
    "vendors/js/markdown-it-container.js"
];

//sync batch loading
await loadScripts(scritpList);

//markdown files to load
const mdFilesList = ["md/cover.md", "md/main.md", "md/back.md"];
//html elements to be filled from converted md file
const partsList = ["cover", 'content', "back"];

//markdownit instanciation (old school method as no ES6 modules are available)
const markdownit = window.markdownit
    ({
        // Options for markdownit
        langPrefix: 'language-fr',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer) //div
    .use(markdownItAttrs, { //custom html element attributes
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });
    
async function loadMdFiles(files) {

    let mdContents = "";
    //!important! forEach can't be used as it doesn't respect await order!
    for (let index = 0; index < files.length; index++) {

        const mdFile = files[index];
        const reponse = await fetch(mdFile);
        const mdContent = await reponse.text();
        //convertion from md to html, returns a string
        //const result = markdownit.render(mdContent); */
        mdContents += mdContent;
    };
    return mdContents;
}
async function layoutHTML() {
    //!important! forEach can't be used as it doesn't respect await order!
    for (let index = 0; index < mdFilesList.length; index++) {

        const mdFile = mdFilesList[index];
        const reponse = await fetch(mdFile);
        const mdContent = await reponse.text();
        //convertion from md to html, returns a string
        const result = markdownit.render(mdContent);

        const destinationElement = document.getElementById(partsList[index]);
        destinationElement.innerHTML = result;
        elementsToPaginate.push(destinationElement.cloneNode(true));
    };
}

function getHTMLFromMD(mdString) {
    //console.log("mdString :\n", mdString);
    const result = markdownit.render(mdString);
    //console.log("result", result);
    return result;
}


previewPrintBt.addEventListener("click", () => {
    const html = getHTMLFromMD(tinyMDE.getContent());
    //console.log("html", html);
    const tempDiv = document.createElement("div");

    tempDiv.innerHTML = html;
    console.log(tempDiv);
    const serializedElements = [...tempDiv.children].map(element => element.outerHTML);
    pagedjsFrame.contentWindow.postMessage(JSON.stringify(serializedElements), '*');
});

printBt.addEventListener("click", (event) => {
    pagedjsFrame.contentWindow.print();
});

//message reception
window.addEventListener("message", (event) => {
    console.log("message", event.source, event.data);
    if (event.data) { }
});

await tinyMDE.setContent(await loadMdFiles(mdFilesList));

